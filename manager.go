package main

import (
	"fmt"
	"log"
	"strconv"
	"time"
)

const (
	KND_TEMP          = "T"
	KND_RELAY         = "R"
	KND_MODE          = "M"
	KND_SOGLIA_GIORNO = "G"
	KND_SOGLIA_NOTTE  = "N"
	KND_REFRESH       = "U"
)

const (
	MODE_MANUAL = iota
	MODE_AUTO
)

type managerMsg struct {
	Kind  string
	Value int
}

type soglia struct {
	temp   int
	ora    int
	minuto int
}

type manager struct {
	t  *termometro
	r  *relay
	br *mqtt

	giorno soglia
	notte  soglia

	mode      int
	lastTemp  int
	lastRelay int
	soglia    *int

	cambiasoglia chan *int
	updatechan   chan managerMsg
	stopchan     chan interface{}

	commandchan chan managerMsg
	webchan     chan managerMsg
}

func initManager(t *termometro, r *relay, br *mqtt, giorno, notte soglia) *manager {
	m := &manager{
		t:            t,
		r:            r,
		br:           br,
		giorno:       giorno,
		notte:        notte,
		mode:         MODE_AUTO,
		lastTemp:     1000,
		soglia:       nil,
		cambiasoglia: make(chan *int),
		updatechan:   make(chan managerMsg),
		stopchan:     make(chan interface{}),
		commandchan:  make(chan managerMsg),
		webchan:      make(chan managerMsg),
	}

	br.addSubTopic("cmnd/caldaia/RELAY", func(topic, message []byte) {
		b, err := strconv.ParseBool(string(message))
		if err != nil {
			log.Printf("received unparsable '%s' on topic %s\n", message, topic)
		} else {
			m.MoveRelay(b)
		}
	})

	br.addSubTopic("cmnd/caldaia/MODE_AUTO", func(topic, message []byte) {
		i, err := strconv.ParseInt(string(message), 10, 32)
		if err != nil {
			log.Printf("received unparsable '%s' on topic %s\n", message, topic)
		} else {
			m.SwitchMode(int(i))
		}
	})

	br.addSubTopic("cmnd/caldaia/SOGLIA_GIORNO", func(topic, message []byte) {
		i, err := strconv.ParseInt(string(message), 10, 32)
		if err != nil {
			log.Printf("received unparsable '%s' on topic %s\n", message, topic)
		} else {
			m.SetSogliaGiorno(int(i))
		}
	})

	br.addSubTopic("cmnd/caldaia/SOGLIA_NOTTE", func(topic, message []byte) {
		i, err := strconv.ParseInt(string(message), 10, 32)
		if err != nil {
			log.Printf("received unparsable '%s' on topic %s\n", message, topic)
		} else {
			m.SetSogliaNotte(int(i))
		}
	})

	go m.updateSoglie()

	return m
}

func (m *manager) SwitchMode(mode int) {
	m.commandchan <- managerMsg{KND_MODE, mode}
}

func (m *manager) Close() {
	m.stopchan <- nil
	m.stopchan <- nil
}

func (m *manager) MoveRelay(state bool) {
	st := 0
	if state {
		st = 1
	}
	m.commandchan <- managerMsg{KND_RELAY, st}
}

func (m *manager) SetSogliaGiorno(temp int) {
	m.commandchan <- managerMsg{KND_SOGLIA_GIORNO, temp}
}

func (m *manager) SetSogliaNotte(temp int) {
	m.commandchan <- managerMsg{KND_SOGLIA_NOTTE, temp}
}

func (m *manager) updateSoglie() {
	ticker := time.NewTicker(time.Minute)
	for {
		select {
		case <-ticker.C:
			now := time.Now()
			oggiGiorno := time.Date(now.Year(), now.Month(), now.Day(), m.giorno.ora, m.giorno.minuto, 0, 0, now.Location())
			oggiNotte := time.Date(now.Year(), now.Month(), now.Day(), m.notte.ora, m.notte.minuto, 0, 0, now.Location())
			if now.Before(oggiGiorno) || now.After(oggiNotte) {
				m.cambiasoglia <- &m.notte.temp
			} else {
				m.cambiasoglia <- &m.giorno.temp
			}
		case <-m.stopchan:
			break
		}
	}
	ticker.Stop()
}

func (m *manager) sendupdate(knd string, val int, channel string, persistent bool) {
	m.updatechan <- managerMsg{knd, val}

	var err error
	if persistent {
		err = m.br.publishPersistent(channel, fmt.Sprint(val))
	} else {
		err = m.br.publish(channel, fmt.Sprint(val))
	}
	if err != nil {
		log.Printf("error publishing update: %s", err.Error())
	}
}

func (m *manager) UpdateLoop() {
	termometro := m.t.GetUpdateChan()
	rele := m.r.GetUpdateChan()
	for {
		select {
		case temp := <-termometro:
			m.lastTemp = temp
			m.sendupdate(KND_TEMP, temp, "stat/caldaia/TEMPERATURE", false)
		case state := <-rele:
			m.lastRelay = 0
			if state {
				m.lastRelay = 1
			}
			m.sendupdate(KND_RELAY, m.lastRelay, "stat/caldaia/RELAY", true)
		case soglia := <-m.cambiasoglia:
			if m.soglia == nil || m.soglia != soglia {
				m.soglia = soglia

				sname := "notte"
				if soglia == &m.giorno.temp {
					sname = "giorno"
				}
				err := m.br.publishPersistent("stat/caldaia/SOGLIA_ATTIVA", fmt.Sprint(sname))
				if err != nil {
					log.Printf("error publishing update: %s", err.Error())
				}
			}
		case cmd := <-m.commandchan:
			switch cmd.Kind {
			case KND_MODE:
				if m.mode != cmd.Value {
					m.mode = cmd.Value
					m.sendupdate(KND_MODE, cmd.Value, "stat/caldaia/MODE_AUTO", true)
				}
			case KND_RELAY:
				if m.mode == MODE_MANUAL {
					b := false
					if cmd.Value != 0 {
						b = true
					}
					m.r.Set(b)
				}
			case KND_SOGLIA_GIORNO:
				if m.giorno.temp != cmd.Value {
					m.giorno.temp = cmd.Value
					m.sendupdate(KND_SOGLIA_GIORNO, cmd.Value, "stat/caldaia/SOGLIA_GIORNO", true)
				}
			case KND_SOGLIA_NOTTE:
				if m.notte.temp != cmd.Value {
					m.notte.temp = cmd.Value
					m.sendupdate(KND_SOGLIA_NOTTE, cmd.Value, "stat/caldaia/SOGLIA_NOTTE", true)
				}
			}
		case cmd := <-m.webchan:
			switch cmd.Kind {
			case KND_MODE:
				m.br.publishPersistent("cmnd/caldaia/MODE_AUTO", fmt.Sprint(cmd.Value))
			case KND_RELAY:
				m.br.publishPersistent("cmnd/caldaia/RELAY", fmt.Sprint(cmd.Value))
			case KND_SOGLIA_GIORNO:
				m.br.publishPersistent("cmnd/caldaia/SOGLIA_GIORNO", fmt.Sprint(cmd.Value))
			case KND_SOGLIA_NOTTE:
				m.br.publishPersistent("cmnd/caldaia/SOGLIA_NOTTE", fmt.Sprint(cmd.Value))
			case KND_REFRESH:
				m.sendupdate(KND_TEMP, m.lastTemp, "stat/caldaia/TEMPERATURE", false)
				m.sendupdate(KND_RELAY, m.lastRelay, "stat/caldaia/RELAY", true)
				m.sendupdate(KND_MODE, m.mode, "stat/caldaia/MODE_AUTO", true)
				m.sendupdate(KND_SOGLIA_GIORNO, m.giorno.temp, "stat/caldaia/SOGLIA_GIORNO", true)
				m.sendupdate(KND_SOGLIA_NOTTE, m.notte.temp, "stat/caldaia/SOGLIA_NOTTE", true)
			}

		case <-m.stopchan:
			return
		}

		if m.mode == MODE_AUTO && m.soglia != nil {
			if m.lastTemp < *m.soglia {
				m.r.Set(true)
			} else {
				m.r.Set(false)
			}
		}
	}
}
