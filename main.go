package main

import (
	"log"
	"os"
	"os/signal"

	gpio "github.com/stianeikeland/go-rpio"
)

const (
	mqttBroker = "openhab.bologna.mellotanica.com:1883"
	mqttID     = "caldaia"
)

func main() {
	err := gpio.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer gpio.Close()

	br, err := getMqtt(mqttBroker, mqttID)
	if err != nil {
		log.Fatal(err)
	}
	defer br.closeMqtt()

	//termometro := initTermometro(11, 9, 10, 8, 0, 1000)
	termometro := initTermometroSpi(0, 0, 1000)
	defer termometro.Close()

	rele := initRelay(25, false)
	defer rele.Close()

	manager := initManager(
		termometro,
		rele,
		br,
		soglia{24, 8, 0},
		soglia{19, 23, 0},
	)
	defer manager.Close()

	br.activateSubscriptions()

	stopchan := make(chan interface{})
	go runServer(manager, stopchan)
	defer func() { stopchan <- nil }()

	go manager.UpdateLoop()

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt, os.Kill)

	for {
		select {
		case s := <-sigchan:
			log.Printf("Received signal: %v\nQuitting...", s)
			return
		}
	}
}
