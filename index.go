package main

import "strconv"

var indexhtml = []byte(`<html>
    <head>
        <title>Caldaia</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <script>
         function init() {
             var sock = new WebSocket("ws://"+window.location.href.replace('http://','').replace(/\/$/,'')+"/sock");

             sock.onmessage = function(event) {
                 var data = JSON.parse(event.data);
                 switch(data.Kind) {
                     case "` + KND_TEMP + `":
                         document.getElementById("temp").innerHTML = ""+data.Value;
                         break;
                     case "` + KND_RELAY + `":
                         if (data.Value == 0) {
                             document.getElementById("releoff").className = "btn btn-lg btn-danger";
                             document.getElementById("releon").className = "btn btn-lg btn-outline-success";
                         } else {
                             document.getElementById("releoff").className = "btn btn-lg btn-outline-danger";
                             document.getElementById("releon").className = "btn btn-lg btn-success";
                         }
                         break;
                     case "` + KND_MODE + `":
                         switch (data.Value) {
                             case ` + strconv.Itoa(MODE_MANUAL) + `:
								document.getElementById("modeauto").className = "btn btn-lg btn-primary";
								document.getElementById("modemanuale").className = "btn btn-lg btn-primary active";
								document.getElementById("releoff").removeAttribute("disabled");
								document.getElementById("releon").removeAttribute("disabled");
								break;
                             case ` + strconv.Itoa(MODE_AUTO) + `:
								document.getElementById("modeauto").className = "btn btn-lg btn-primary active";
								document.getElementById("modemanuale").className = "btn btn-lg btn-primary";
								document.getElementById("releoff").setAttribute("disabled", "");
								document.getElementById("releon").setAttribute("disabled", "");
                                break;
                         }
                         break;
                     case "` + KND_SOGLIA_GIORNO + `":
                         document.getElementById("sday").innerHTML = ""+data.Value;
                         break;
                     case "` + KND_SOGLIA_NOTTE + `":
                         document.getElementById("snight").innerHTML = ""+data.Value;
                         break;
                 }
             }

             sock.onopen = function(event) {
                 document.getElementById("day+").onclick = function() {
                     var newtemp = Number(document.getElementById("sday").innerHTML) + 1;
                     sock.send(JSON.stringify({"Kind": "` + KND_SOGLIA_GIORNO + `", "Value": newtemp}));
                 };

                 document.getElementById("day-").onclick = function() {
                     var newtemp = Number(document.getElementById("sday").innerHTML) - 1;
                     sock.send(JSON.stringify({"Kind": "` + KND_SOGLIA_GIORNO + `", "Value": newtemp}));
                 }

                 document.getElementById("night+").onclick = function() {
                     var newtemp = Number(document.getElementById("snight").innerHTML) + 1;
                     sock.send(JSON.stringify({"Kind": "` + KND_SOGLIA_NOTTE + `", "Value": newtemp}));
                 };

                 document.getElementById("night-").onclick = function() {
                     var newtemp = Number(document.getElementById("snight").innerHTML) - 1;
                     sock.send(JSON.stringify({"Kind": "` + KND_SOGLIA_NOTTE + `", "Value": newtemp}));
                 }

                 document.getElementById("modemanuale").onclick = function() {
                     sock.send(JSON.stringify({"Kind": "` + KND_MODE + `", "Value": ` + strconv.Itoa(MODE_MANUAL) + `}));
                 }

                 document.getElementById("modeauto").onclick = function() {
                     sock.send(JSON.stringify({"Kind": "` + KND_MODE + `", "Value": ` + strconv.Itoa(MODE_AUTO) + `}));
                 }

                 document.getElementById("releoff").onclick = function() {
                     sock.send(JSON.stringify({"Kind": "` + KND_RELAY + `", "Value": 0}));
                 }

                 document.getElementById("releon").onclick = function() {
                     sock.send(JSON.stringify({"Kind": "` + KND_RELAY + `", "Value": 1}));
                 }

                 sock.send(JSON.stringify({"Kind": "` + KND_REFRESH + `", "Value": 0}));
             }
         }
         window.onload = init;
        </script>
        <div id="maindiv" style="background-color: rgb(200, 200, 200); border-radius: 5px; margin: 10px 10px 10px 10px; padding: 1px 10px 10px 10px; display: inline-block;">
            <h1 class="featurette-heading">Termostato</h1>
            <p>Temperatura: <span id=temp></span>°</p>
            <br/>
            <p>Temperatura giorno (7 - 00): <span id=sday></span><br/>
                <button id="day+" class="btn btn-lg btn-primary" type="button" style="min-width: 100px">+</button> <button id="day-" class="btn btn-lg btn-primary" type="button" style="min-width: 100px">-</button>
            </p>
            <p>Temperatura notte (00 - 7): <span id=snight></span><br/>
                <button id="night+" class="btn btn-lg btn-primary" type="button" style="min-width: 100px">+</button> <button id="night-" class="btn btn-lg btn-primary" type="button" style="min-width: 100px">-</button>
            </p>
            <div id="mode" class="btn-group btn-group-toggle" role="group" aria-label="...">
                <button id="modeauto" type="button" class="btn btn-lg btn-primary"> Auto </button>
                <button id="modemanuale" type="button" class="btn btn-lg btn-primary"> Manuale </button>
            </div><br/>
            <div id="rele" class="btn-group btn-group-toggle" role="group" aria-label="...">
                <button id="releon" type="button" class="btn btn-lg btn-primary"> On </button>
                <button id="releoff" type="button" class="btn btn-lg btn-primary"> Off </button>
            </div><br/>
        </div>
    </body>
</html>`)
