package main

import (
	"math"
	"time"

	gpio "github.com/stianeikeland/go-rpio"
)

const (
	cycles   = 5
	meanvals = 5
)

type termometroSw struct {
	clk  gpio.Pin
	miso gpio.Pin
	mosi gpio.Pin
	cs   gpio.Pin
}

type termometroHw struct {
	spi gpio.SpiDev
}

type termometro struct {
	hw *termometroHw
	sw *termometroSw

	adc int

	update   chan int
	stopchan chan interface{}

	last    int
	latest  [meanvals]float64
	latestI int
}

func initTermometro(clk, miso, mosi, cs, adc, refreshms int) *termometro {
	if adc < 0 || adc > 7 {
		return nil
	}

	t := &termometro{
		sw: &termometroSw{
			clk:  gpio.Pin(clk),
			miso: gpio.Pin(miso),
			mosi: gpio.Pin(mosi),
			cs:   gpio.Pin(cs),
		},
		hw:       nil,
		adc:      adc,
		last:     -300,
		latest:   [meanvals]float64{-300},
		latestI:  0,
		update:   make(chan int),
		stopchan: make(chan interface{}),
	}

	t.sw.clk.Output()
	t.sw.miso.Input()
	t.sw.mosi.Output()
	t.sw.cs.Output()

	go t.loop(refreshms)

	return t
}

func initTermometroSpi(spi, adc, refreshms int) *termometro {
	if adc < 0 || adc > 7 {
		return nil
	}

	t := &termometro{
		sw: nil,
		hw: &termometroHw{
			spi: gpio.SpiDev(spi),
		},
		adc:      adc,
		last:     -300,
		latest:   [meanvals]float64{-300},
		latestI:  0,
		update:   make(chan int),
		stopchan: make(chan interface{}),
	}

	gpio.SpiBegin(t.hw.spi)
	gpio.SpiSpeed(1000000)

	go t.loop(refreshms)

	return t
}

func (t *termometro) loop(refreshms int) {
	ticker := time.NewTicker(time.Duration(refreshms) * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			t.latest[t.latestI] = t.Read()

			acc := 0.0
			cnt := 0.0

			for _, v := range t.latest {
				if v > -300 {
					acc += v
					cnt++
				}
			}

			temp := int(math.Round(acc / cnt))

			if t.update != nil && t.last != temp {
				t.update <- temp
			}
			t.last = temp

			t.latestI = (t.latestI + 1) % meanvals
		case <-t.stopchan:
			break
		}
	}
	ticker.Stop()
}

func (t *termometro) getVal() float64 {
	cmd := (t.adc | 0x18) << 3
	adcout := 0
	if t.sw != nil {
		t.sw.cs.High()

		t.sw.clk.Low()
		t.sw.cs.Low()

		for i := 0; i < 5; i++ {
			if (cmd & 0x80) != 0 {
				t.sw.mosi.High()
			} else {
				t.sw.mosi.Low()
			}
			cmd <<= 1
			t.sw.clk.High()
			t.sw.clk.Low()
		}

		//read in one empty bit, one null bit and 10 adc bits
		for i := 0; i < 12; i++ {
			t.sw.clk.High()
			t.sw.clk.Low()
			adcout <<= 1
			if (t.sw.miso.Read()) != 0 {
				adcout |= 1
			}
		}

		t.sw.cs.High()

		adcout >>= 1
	} else {
		data := []byte{byte(cmd), 0, 0}
		gpio.SpiExchange(data)

		adcout = int(data[0]&1) << 9
		adcout |= int(data[1]) << 1
		adcout |= int(data[2]&0x80) >> 7
		adcout &= 0x3FF
	}
	return float64(adcout)
}

func (t *termometro) GetUpdateChan() chan int {
	return t.update
}

func (t *termometro) Read() float64 {
	total := float64(0)
	for i := 0; i < cycles; i++ {
		tt := ((t.getVal() * 3300 / 1023) / 10) - 273
		total += tt
	}
	return total / cycles
}

func (t *termometro) Close() {
	t.stopchan <- nil
	if t.hw != nil {
		gpio.SpiEnd(t.hw.spi)
	}
}
