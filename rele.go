package main

import (
	gpio "github.com/stianeikeland/go-rpio"
)

type relayCb func(newstate bool)

type relay struct {
	ctrl gpio.Pin

	state    bool
	offstate bool

	update   chan bool
	command  chan bool
	stopchan chan interface{}
}

func initRelay(pin int, offstate bool) *relay {
	rl := &relay{
		ctrl:     gpio.Pin(pin),
		state:    true,
		offstate: offstate,
		update:   make(chan bool),
		command:  make(chan bool),
		stopchan: make(chan interface{}),
	}

	rl.ctrl.Output()

	go rl.loop()
	rl.Set(false)

	return rl
}

func (r *relay) Set(state bool) {
	r.command <- state
}

func (r *relay) Get() bool {
	return r.state
}

func (r *relay) GetUpdateChan() chan bool {
	return r.update
}

func (r *relay) loop() {
	for {
		select {
		case state := <-r.command:
			if r.state != state {
				if state != r.offstate {
					r.ctrl.High()
				} else {
					r.ctrl.Low()
				}
				r.state = state

				if r.update != nil {
					r.update <- state
				}
			}
		case <-r.stopchan:
			break
		}
	}
}

func (r *relay) Close() {
	r.stopchan <- nil
}
