package main

import (
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var pages []*websocket.Conn
var pagesLock sync.Mutex

func handleWs(w http.ResponseWriter, r *http.Request, rcvhdl func(c *websocket.Conn)) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	pagesLock.Lock()
	pages = append(pages, conn)
	pagesLock.Unlock()

	go rcvhdl(conn)
}

func runServer(m *manager, stopchan chan interface{}) {
	pages = make([]*websocket.Conn, 0)
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(indexhtml)
	})
	r.HandleFunc("/sock", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("requested websocket\n")
		handleWs(w, r, func(c *websocket.Conn) {
			var cmd managerMsg
			for {
				err := c.ReadJSON(&cmd)
				if err != nil {
					log.Println(err)
					switch err.(type) {
					case *websocket.CloseError:

						pagesLock.Lock()
						for i, p := range pages {
							if p == c {
								pages = append(pages[:i], pages[i+1:]...)
								break
							}
						}
						pagesLock.Unlock()
						return
					}
				}

				m.webchan <- cmd
			}
		})
	})
	go func() {
		for {
			select {
			case update := <-m.updatechan:
				for _, p := range pages {
					err := p.WriteJSON(update)
					if err != nil {
						log.Println(err)
					}
				}
			case <-stopchan:
				return
			}
		}
	}()

	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:80",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Printf("starting webserver on port 80")
	log.Fatal(srv.ListenAndServe())
}
