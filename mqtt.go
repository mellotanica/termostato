package main

import (
	"log"

	mqttlib "github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
)

type mqtt struct {
	cli    *client.Client
	topics []*client.SubReq
}

func getMqtt(broker, id string) (*mqtt, error) {
	log.Printf("connecting to MQTT broker '%s' with ID '%s'\n", broker, id)
	// Create an MQTT Client.
	cli := client.New(&client.Options{
		// Define the processing of the error handler.
		ErrorHandler: func(err error) {
			log.Println(err)
		},
	})

	// Connect to the MQTT Server.
	err := cli.Connect(&client.ConnectOptions{
		Network:  "tcp",
		Address:  broker,
		ClientID: []byte(id),
	})
	if err != nil {
		return nil, err
	}

	log.Printf("connected to MQTT broker")

	return &mqtt{cli, make([]*client.SubReq, 0)}, nil
}

func (m *mqtt) closeMqtt() {
	err := m.cli.Disconnect()
	if err != nil {
		log.Printf("unable to disconnect client: %s\n", err.Error())
	}

	m.cli.Terminate()
}

func (m *mqtt) publish(topic, message string) error {
	return m.cli.Publish(&client.PublishOptions{
		QoS:       mqttlib.QoS1,
		TopicName: []byte(topic),
		Message:   []byte(message),
	})
}

func (m *mqtt) publishPersistent(topic, message string) error {
	return m.cli.Publish(&client.PublishOptions{
		QoS:       mqttlib.QoS1,
		Retain:    true,
		TopicName: []byte(topic),
		Message:   []byte(message),
	})
}

func (m *mqtt) removePersistent(topic string) error {
	return m.publishPersistent(topic, "")
}

func (m *mqtt) addSubTopic(topic string, handler client.MessageHandler) {
	m.topics = append(m.topics, &client.SubReq{
		TopicFilter: []byte(topic),
		QoS:         mqttlib.QoS1,
		Handler:     handler,
	})
}

func (m *mqtt) activateSubscriptions() error {
	return m.cli.Subscribe(&client.SubscribeOptions{
		SubReqs: m.topics,
	})
}
